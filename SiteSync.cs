﻿//!CompilerOption:Optimize:On
//!CompilerOption:AddRef:Newtonsoft.Json.dll
//!CompilerOption:AddRef:System.Management.dll

using System;
using System.Text;
using System.Management;
using System.Security.Cryptography;
using Styx.Common;
using StatsCollector.JSONRPC2;
using System.Threading.Tasks;
using StatsCollector.Query;

namespace StatsCollector
{
    public class MainResponse
    {
        public bool success;
    }
    /*
    Класс предоставляет API для заливания всякой инфы на сайт. Потом будет предоставлять ещё API для управления с сайта. Наверное.
        */
    public class SiteSync
    {
        //SINGLETON ??? OMFG... F**k c#.
        public static SiteSync Instance { get; private set; }
        protected static JsonService m_service;

        static SiteSync()
        {
            Instance = new SiteSync();
            m_service = new JsonService();
        }

        const int MACHINE_NOT_FOUND = -20000;
        const int LOGIN_FAILED = -20001;
        const int SAVE_FAILED = -20002;
        const int ACCESS_ERROR = -20003;
        const int VALIDATE_ERROR = -20004;

        private String auth_token;
        public String AuthToken
        {
            get { return auth_token; }
        }
        private static String m_deviceId = generateId();
        public static String DeviceId
        {
            get { return m_deviceId; }
        }
        public string Address
        {
            get { return GlobalSettings.Instance.SyncAddress; }
        }
        public string Login
        {
            get { return GlobalSettings.Instance.SyncLogin; }
        }

        public string MachineName
        {
            get { return GlobalSettings.Instance.SyncMachineName; }
        }

        private async Task<bool> InitAsync( Action<bool> _returned = null)
        {

            Recive<AuthResponse, Error> res = null;
            try
            {
                res = await m_service.Post<Recive<AuthResponse, Error>>(Address,
                    new Send("register-machine",
                    new AuthQuery())
                );
            }
            catch (Exception Ex)
            {
                Logging.Write($"Error on send/build/recive query : {Ex}");
                _returned?.Invoke(false);
                return false;
            }

            if (res == null)
            {
                Logging.Write("Undefined error on auth.");
                _returned?.Invoke(false);
                return false;
            }

            if (res.Error != null)
            {
                LogError(res);
                _returned?.Invoke(false);
                return false;
            }

            auth_token = res.Result.identy;
            Logging.Write(LogLevel.Normal, $"Current server id : {DeviceId} ");
            Logging.Write(LogLevel.Normal, $"Authorized with key : {auth_token}");
            _returned?.Invoke(true);
            return true;
        }

        public async Task<bool> Init()
        {
            Logging.Write(LogLevel.Verbose, $"Login {Login} :: {MachineName}");
            return await InitAsync();
        }

        bool LogError<T>(Recive<T, Error> result)
        {
            if (result.Error != null)
            {
                switch (result.Error.Code)
                {
                    case MACHINE_NOT_FOUND:
                        Logging.Write(LogLevel.Normal, "Not registered server. Pls relogin.");
                        //Init();
                        break;
                    case LOGIN_FAILED:
                        this.auth_token = null;
                        Logging.Write(LogLevel.Normal, "Login failed. Retry again.");
                        break;
                    case SAVE_FAILED:
                        Logging.Write(LogLevel.Normal, "Error on save 'bot'.Pls contact me...");
                        break;
                    case ACCESS_ERROR:
                        Logging.Write(LogLevel.Normal, $"You not access this member -> {result.Error.Message}");
                        break;
                    default:
                        Logging.Write(LogLevel.Normal, $"Undefined error code : {result.Error.Code}");
                        break;
                }
                return true;
            }
            return false;
        }

        public async void sendStatistic( )
        {
            if (auth_token == null)
                if ( !await InitAsync() )
                    return;

            await m_service.Post<Recive<MainResponse, Error>>(Address,
                new Send("botinfo", new StatisticQuery() ),
                response =>
                {
                    if (response.Error != null)
                        Instance.LogError(response);
                },
                error =>
                {
                    Logging.Write("Error sync with StatsCollector site.");
                    auth_token = null;
                }
            );
        }

        //найти событие при бане, но меня пока не банило (
        public async void sendErrorBot()
        {
            if (auth_token == null)
                if (!await InitAsync())
                    return;
            await m_service.Post<Recive<MainResponse, Error>>(Address,
                new Send("botbanned",new BaseBotQuery() ),
                response =>
                {
                    if (response.Error != null)
                        Instance.LogError(response);
                },
                error =>
                {
                    Logging.Write("Error sync with StatsCollector site.");
                    auth_token = null;
                }
                );
        }

        public static String generateId()
        {
            using (MD5 md5Hash = MD5.Create())
                return GetMd5Hash( md5Hash, GetCPUId() + GetMACAddress() + GetVolumeSerial() ) ;
        }


        // честно скомуниздили http://www.nullskull.com/articles/20030511.asp

        /// <summary>
        /// return Volume Serial Number from hard drive
        /// </summary>
        /// <param name="strDriveLetter">[optional] Drive letter</param>
        /// <returns>[string] VolumeSerialNumber</returns>
        public static string GetVolumeSerial(string strDriveLetter = "C")
        {
            if (strDriveLetter == "" || strDriveLetter == null) strDriveLetter = "C";
            ManagementObject disk =
                new ManagementObject("win32_logicaldisk.deviceid=\"" + strDriveLetter + ":\"");
            disk.Get();
            return disk["VolumeSerialNumber"].ToString();
        }

        /// <summary>
        /// Returns MAC Address from first Network Card in Computer
        /// </summary>
        /// <returns>[string] MAC Address</returns>
        public static string GetMACAddress()
        {
            ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            ManagementObjectCollection moc = mc.GetInstances();
            string MACAddress = String.Empty;
            foreach (ManagementObject mo in moc)
            {
                if (MACAddress == String.Empty)  // only return MAC Address from first card
                {
                    if ((bool)mo["IPEnabled"] == true) MACAddress = mo["MacAddress"].ToString();
                }
                mo.Dispose();
            }
            MACAddress = MACAddress.Replace(":", "");
            return MACAddress;
        }
        /// <summary>
        /// Return processorId from first CPU in machine
        /// </summary>
        /// <returns>[string] ProcessorId</returns>
        public static string GetCPUId()
        {
            string cpuInfo = String.Empty;
            string temp = String.Empty;
            ManagementClass mc = new ManagementClass("Win32_Processor");
            ManagementObjectCollection moc = mc.GetInstances();
            foreach (ManagementObject mo in moc)
            {
                if (cpuInfo == String.Empty)
                {// only return cpuInfo from first CPU
                    cpuInfo = mo.Properties["ProcessorId"].Value.ToString();
                }
            }
            return cpuInfo;
        }

        //опять комуниздим https://msdn.microsoft.com/ru-ru/library/system.security.cryptography.md5(v=vs.110).aspx
        static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

    }
    
}
