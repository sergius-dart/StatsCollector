﻿using System;
using System.Text;
using Newtonsoft.Json;
using StatsCollector.JSONRPC2;
using System.Net.Http;
using System.Threading.Tasks;

namespace StatsCollector
{
    public class JsonService 
    {
        HttpClient _client = new HttpClient();
        protected JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            TypeNameHandling = Newtonsoft.Json.TypeNameHandling.All
        };
        /// <summary>
        /// Gets the specified URL.
        /// </summary>
        /// <typeparam name="TResponse">The type of the attribute response.</typeparam>
        /// <param name="url">The URL.</param>
        /// <param name="onComplete">The configuration complete.</param>
        /// <param name="onError">The configuration error.</param>
        public async Task<TResponse> Get<TResponse>(string url, Action<TResponse> onComplete = null, Action<Exception> onError = null)
        {
            HttpResponseMessage response = await _client.GetAsync(url);
            TResponse result = default(TResponse);
            try
            {
                if (response.IsSuccessStatusCode)
                {
                    string result_str = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<TResponse>(result_str, settings);
                    onComplete?.Invoke(result);
                }
            } catch (Exception e)
            {
                onError?.Invoke(e);
            }
            return result;
        }
        /// <summary>
        /// Posts the specified URL.
        /// </summary>
        /// <typeparam name="TResponse">The type of the attribute response.</typeparam>
        /// <param name="url">The URL.</param>
        /// <param name="jsonData">The json data.</param>
        /// <param name="onComplete">The configuration complete.</param>
        /// <param name="onError">The configuration error.</param>
        public async Task<TResponse> Post<TResponse>(string url, Base jsonData, Action<TResponse> onComplete = null, Action<Exception> onError = null)
        {

            HttpResponseMessage response = await _client.PostAsync(
                url,
               new StringContent( 
                    JsonConvert.SerializeObject(jsonData),
                    Encoding.UTF8,
                    "application/json") 
            );
            TResponse result = default(TResponse);
            try
            {
                if (response.IsSuccessStatusCode)
                {
                    string result_str = await response.Content.ReadAsStringAsync();
                    result = JsonConvert.DeserializeObject<TResponse>(result_str, settings);
                    onComplete?.Invoke(result);
                }
            }
            catch (Exception e)
            {
                onError?.Invoke(e);
            }
            return result;
        }
    }
}
