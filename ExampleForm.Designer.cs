﻿namespace StatsCollector
{
    partial class ExampleForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MoveWindowBorder = new System.Windows.Forms.Label();
            this.hide_btn = new System.Windows.Forms.Label();
            this.address_lbl = new System.Windows.Forms.Label();
            this.login_lbl = new System.Windows.Forms.Label();
            this.pass_lbl = new System.Windows.Forms.Label();
            this.machine_name_lbl = new System.Windows.Forms.Label();
            this.save_btn = new System.Windows.Forms.Button();
            this.address = new System.Windows.Forms.TextBox();
            this.login = new System.Windows.Forms.TextBox();
            this.pass = new System.Windows.Forms.TextBox();
            this.machine_name = new System.Windows.Forms.TextBox();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.secHash = new System.Windows.Forms.TextBox();
            this.destroyName = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // MoveWindowBorder
            // 
            this.MoveWindowBorder.BackColor = System.Drawing.Color.Gray;
            this.MoveWindowBorder.Dock = System.Windows.Forms.DockStyle.Top;
            this.MoveWindowBorder.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MoveWindowBorder.Location = new System.Drawing.Point(0, 0);
            this.MoveWindowBorder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.MoveWindowBorder.Name = "MoveWindowBorder";
            this.MoveWindowBorder.Size = new System.Drawing.Size(637, 28);
            this.MoveWindowBorder.TabIndex = 0;
            this.MoveWindowBorder.Text = "   Settings";
            this.MoveWindowBorder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MoveWindowBorder.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MoveWindowBorder_MouseDown);
            // 
            // hide_btn
            // 
            this.hide_btn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.hide_btn.AutoSize = true;
            this.hide_btn.BackColor = System.Drawing.Color.Gray;
            this.hide_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.hide_btn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hide_btn.ForeColor = System.Drawing.Color.Red;
            this.hide_btn.Location = new System.Drawing.Point(608, 4);
            this.hide_btn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.hide_btn.Name = "hide_btn";
            this.hide_btn.Size = new System.Drawing.Size(21, 20);
            this.hide_btn.TabIndex = 1;
            this.hide_btn.Text = "X";
            this.hide_btn.Click += new System.EventHandler(this.hide_btn_Click);
            // 
            // address_lbl
            // 
            this.address_lbl.AutoSize = true;
            this.address_lbl.Location = new System.Drawing.Point(17, 37);
            this.address_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.address_lbl.Name = "address_lbl";
            this.address_lbl.Size = new System.Drawing.Size(133, 17);
            this.address_lbl.TabIndex = 2;
            this.address_lbl.Text = "JSON-RPC address";
            // 
            // login_lbl
            // 
            this.login_lbl.AutoSize = true;
            this.login_lbl.Location = new System.Drawing.Point(17, 66);
            this.login_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.login_lbl.Name = "login_lbl";
            this.login_lbl.Size = new System.Drawing.Size(43, 17);
            this.login_lbl.TabIndex = 3;
            this.login_lbl.Text = "Login";
            // 
            // pass_lbl
            // 
            this.pass_lbl.AutoSize = true;
            this.pass_lbl.Location = new System.Drawing.Point(17, 96);
            this.pass_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.pass_lbl.Name = "pass_lbl";
            this.pass_lbl.Size = new System.Drawing.Size(69, 17);
            this.pass_lbl.TabIndex = 4;
            this.pass_lbl.Text = "Password";
            // 
            // machine_name_lbl
            // 
            this.machine_name_lbl.AutoSize = true;
            this.machine_name_lbl.Location = new System.Drawing.Point(17, 126);
            this.machine_name_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.machine_name_lbl.Name = "machine_name_lbl";
            this.machine_name_lbl.Size = new System.Drawing.Size(102, 17);
            this.machine_name_lbl.TabIndex = 5;
            this.machine_name_lbl.Text = "Machine Name";
            // 
            // save_btn
            // 
            this.save_btn.Location = new System.Drawing.Point(20, 217);
            this.save_btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(135, 28);
            this.save_btn.TabIndex = 6;
            this.save_btn.Text = "Check and save";
            this.save_btn.UseVisualStyleBackColor = true;
            this.save_btn.Click += new System.EventHandler(this.save_btn_Click);
            // 
            // address
            // 
            this.address.Location = new System.Drawing.Point(160, 33);
            this.address.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(460, 22);
            this.address.TabIndex = 7;
            // 
            // login
            // 
            this.login.Location = new System.Drawing.Point(160, 63);
            this.login.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(460, 22);
            this.login.TabIndex = 8;
            // 
            // pass
            // 
            this.pass.Location = new System.Drawing.Point(160, 93);
            this.pass.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pass.Name = "pass";
            this.pass.Size = new System.Drawing.Size(460, 22);
            this.pass.TabIndex = 9;
            // 
            // machine_name
            // 
            this.machine_name.Location = new System.Drawing.Point(160, 123);
            this.machine_name.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.machine_name.Name = "machine_name";
            this.machine_name.Size = new System.Drawing.Size(460, 22);
            this.machine_name.TabIndex = 10;
            // 
            // cancel_btn
            // 
            this.cancel_btn.Location = new System.Drawing.Point(520, 217);
            this.cancel_btn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(100, 28);
            this.cancel_btn.TabIndex = 11;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 156);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 17);
            this.label1.TabIndex = 12;
            this.label1.Text = "Unique Hash";
            // 
            // secHash
            // 
            this.secHash.Location = new System.Drawing.Point(160, 153);
            this.secHash.Margin = new System.Windows.Forms.Padding(4);
            this.secHash.Name = "secHash";
            this.secHash.Size = new System.Drawing.Size(460, 22);
            this.secHash.TabIndex = 13;
            // 
            // destroyName
            // 
            this.destroyName.AutoSize = true;
            this.destroyName.Location = new System.Drawing.Point(160, 182);
            this.destroyName.Name = "destroyName";
            this.destroyName.Size = new System.Drawing.Size(186, 21);
            this.destroyName.TabIndex = 14;
            this.destroyName.Text = "Destroy Character Name";
            this.destroyName.UseVisualStyleBackColor = true;
            // 
            // ExampleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 258);
            this.Controls.Add(this.destroyName);
            this.Controls.Add(this.secHash);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.machine_name);
            this.Controls.Add(this.pass);
            this.Controls.Add(this.login);
            this.Controls.Add(this.address);
            this.Controls.Add(this.save_btn);
            this.Controls.Add(this.machine_name_lbl);
            this.Controls.Add(this.pass_lbl);
            this.Controls.Add(this.login_lbl);
            this.Controls.Add(this.address_lbl);
            this.Controls.Add(this.hide_btn);
            this.Controls.Add(this.MoveWindowBorder);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "ExampleForm";
            this.Text = "Settings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label MoveWindowBorder;
        private System.Windows.Forms.Label hide_btn;
        private System.Windows.Forms.Label address_lbl;
        private System.Windows.Forms.Label login_lbl;
        private System.Windows.Forms.Label pass_lbl;
        private System.Windows.Forms.Label machine_name_lbl;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.TextBox login;
        private System.Windows.Forms.TextBox pass;
        private System.Windows.Forms.TextBox machine_name;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox secHash;
        private System.Windows.Forms.CheckBox destroyName;
    }
}