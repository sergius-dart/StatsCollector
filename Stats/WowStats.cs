﻿using System;
using Styx;
using Styx.CommonBot;
using Styx.WoWInternals;
using Styx.CommonBot.Frames;

namespace StatsCollector.Stats
{
    class WowStats
    {
        public ulong gold
        {
            get {
                if (StyxWoW.Me.Gold > 10 * 1000 * 1000)
                {
                    TreeRoot.Shutdown();
                    throw new Exception("too many gold - restart hb");
                }
                else
                    return StyxWoW.Me.Gold;
            }
        }

        public ulong silver => StyxWoW.Me.Silver;

        public ulong copper => StyxWoW.Me.Copper;

        public float level => StyxWoW.Me.LevelFraction;

        public uint guild_level => StyxWoW.Me.GuildLevel;

        public int faction_group => (int)StyxWoW.Me.FactionGroup;
    }
}
