﻿using Styx.CommonBot;

namespace StatsCollector.Stats
{
    class HbStats
    {
        public uint BGsCompleted
        {
            get { return GameStats.BGsCompleted; }
        }
        public uint BGsLost
        {
            get { return GameStats.BGsLost; }
        }
        public float BGsLostPerHour
        {
            get { return GameStats.BGsLostPerHour; }
        }
        public float BGsPerHour
        {
            get { return GameStats.BGsPerHour; }
        }
        public uint BGsWon
        {
            get { return GameStats.BGsWon; }
        }
        public float BGsWonPerHour
        {
            get { return GameStats.BGsWonPerHour; }
        }
        public uint Deaths
        {
            get { return GameStats.Deaths; }
        }
        public float DeathsPerHour
        {
            get { return GameStats.DeathsPerHour; }
        }
        public float GoldGained
        {
            get { return GameStats.GoldGained; }
        }
        public float GoldPerHour
        {
            get { return GameStats.GoldPerHour; }
        }
        public uint HonorGained
        {
            get { return GameStats.HonorGained; }
        }
        public float HonorPerHour
        {
            get { return GameStats.HonorPerHour; }
        }
        public bool IsMeasuring
        {
            get { return GameStats.IsMeasuring; }
        }
        public uint Loots
        {
            get { return GameStats.Loots; }
        }
        public float LootsPerHour
        {
            get { return GameStats.LootsPerHour; }
        }
        public uint MobsKilled
        {
            get { return GameStats.MobsKilled; }
        }
        public float MobsPerHour
        {
            get { return GameStats.MobsPerHour; }
        }
        public float XPPerHour
        {
            get { return GameStats.XPPerHour; }
        }
    }
}
