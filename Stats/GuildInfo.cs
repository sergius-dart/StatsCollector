﻿using Styx;
using Styx.WoWInternals;
using System;

namespace StatsCollector.Stats
{
    class GuildInfo
    {
        public static Int64 GBGOLD;
        public static string CachedGuildName;
        public string name => StyxWoW.Me.GuildLevel > 0 ? Lua.GetReturnVal<string>("return GetGuildInfo(\"player\") ", 0) : CachedGuildName;
        public Int64 gold => GBGOLD;
    }
}
