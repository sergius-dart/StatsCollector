﻿using System;
using Newtonsoft.Json;

namespace StatsCollector.JSONRPC2
{
    public class Base
    {
        protected static Random rnd;
        [JsonProperty(PropertyName = "jsonrpc")]
        public string Jsonrpc { get; set; }
        //        public string jsonrpc;  //version
        [JsonProperty(PropertyName = "id")]
        public int Id;
        protected Base()
        {
            if (rnd == null)
                rnd = new Random();
            Id = rnd.Next();//generate random id. Для безопасности.
            Jsonrpc = "2.0";//принудительно выставляем 2-ю версию. Пока так.
        }
    }
    public class Send: Base
    {
        [JsonProperty(PropertyName = "method")]
        public String Method;
        [JsonProperty(PropertyName = "params")]
        public object Params;
        public Send(String method, object @params)
        {
            Method = method;
            Params = @params;
        }

}
    public class Error
    {
        [JsonProperty(PropertyName = "code")]
        public int Code;
        [JsonProperty(PropertyName = "message")]
        public String Message;
    }
    public class Recive<T, E>:Base
    {
        [JsonProperty(PropertyName = "result")]
        public T Result;
        [JsonProperty(PropertyName = "error")]
        public E Error;
    }
}
