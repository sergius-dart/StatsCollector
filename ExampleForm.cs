﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace StatsCollector
{
    public partial class ExampleForm : Form
    {

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public ExampleForm()
        {
            InitializeComponent();
            LoadValues();
        }

        private void MoveWindowBorder_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void hide_btn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private async void save_btn_Click(object sender, EventArgs e)
        {
            StoreValues();
            if (await SiteSync.Instance.Init())
            {
                MessageBox.Show("All ok! Statistics sends");
                StatsCollectorPlugin.Instance.Pulse();
                GlobalSettings.Instance.Save();
                Close();
            }
            else
                MessageBox.Show("Bad input");

        }

        private void LoadValues()
        {
            address.Text = GlobalSettings.Instance.SyncAddress;
            login.Text = GlobalSettings.Instance.SyncLogin;
            pass.Text = GlobalSettings.Instance.SyncPassword;
            machine_name.Text = GlobalSettings.Instance.SyncMachineName;
            secHash.Text = GlobalSettings.Instance.UniqDestoryHash;
            destroyName.Checked = GlobalSettings.Instance.DestoryCharacterName;

        }

        private void StoreValues()
        {
            GlobalSettings.Instance.SyncAddress = address.Text;
            GlobalSettings.Instance.SyncLogin = login.Text;
            GlobalSettings.Instance.SyncPassword = pass.Text;
            GlobalSettings.Instance.SyncMachineName = machine_name.Text;
            GlobalSettings.Instance.UniqDestoryHash = secHash.Text;
            GlobalSettings.Instance.DestoryCharacterName = destroyName.Checked;
        }
    }
}
