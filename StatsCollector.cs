//!CompilerOption:Optimize:On
//!CompilerOption:AddRef:Newtonsoft.Json.dll
//!CompilerOption:AddRef:PresentationFramework.dll
//!CompilerOption:AddRef:WindowsBase.dll
//!CompilerOption:AddRef:System.Net.Http.dll
using Styx.Common;
using Styx.Plugins;
using System;
using System.Windows.Media;
using Styx.CommonBot.Frames;
using Styx.WoWInternals;
using StatsCollector.Stats;

namespace StatsCollector
{
    public class StatsCollectorPlugin : HBPlugin
    {
        private DateTime _nextSend = DateTime.Now;
        private int minutesSleep = 1;

        private bool isEnable = true;

        static internal StatsCollectorPlugin Instance { get; private set; }
        public StatsCollectorPlugin()
        {
            Instance = this;
        }

        #region Plugin
        public override string Author => "sergius-dart";

        public override string Name => "StatsCollector";

        public override System.Version Version => new System.Version(1, 0, 1);

        public override string ButtonText => "SyncSettings";


        public override void Pulse()
        {
            if (!isEnable)
                return;

            if (new Frame("GuildBankFrame").IsVisible)
            {
                GuildInfo.GBGOLD = Convert.ToInt64(Lua.GetReturnVal<string>("return GetGuildBankMoney()", 0)) / 10000;
                var gname = Lua.GetReturnVal<string>("return GetGuildInfo(\"player\") ", 0);
                if (!String.IsNullOrEmpty(gname)) GuildInfo.CachedGuildName = gname;
            }

            if (GlobalSettings.Instance?.SyncAddress?.Length == 0)
                return;

            if ((_nextSend - DateTime.Now).TotalMinutes > 0) return;
            try
            {
                if(GuildInfo.GBGOLD != 0)Logging.Write(Colors.Gold,$" Current GB gold: {GuildInfo.GBGOLD}");
                SiteSync.Instance.sendStatistic();
            }
            catch (Exception ex)
            {
                Logging.Write("Exception on send or build query : {0} ", ex);
            }
            _nextSend = DateTime.Now.AddMinutes(minutesSleep);
            GuildInfo.GBGOLD = 0;
        }

        private void Shutdown()
        {

        }

        public override bool WantButton => true;

        public override void OnButtonPress()
        {
            try
            {
                (new ExampleForm()).Show();
            } catch(Exception E)
            {
                Logging.Write("Exception {1} {0}", E.StackTrace, E.Message);
            }
        }


        #endregion

        public override void OnEnable()
        {
            isEnable = true;
        }

        public override void OnDisable()
        {
            isEnable = false;
        }
    }
}