﻿using System;
using Styx;
using System.Security.Cryptography;
using System.Text;

namespace StatsCollector.Query
{
    class BaseBotQuery:BaseSecureQuery
    {
        public String characterName
        {
            get {
                using (MD5 md5Hash = MD5.Create())
                {
                    byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(GlobalSettings.Instance.UniqDestoryHash + StyxWoW.Me.Name + GlobalSettings.Instance.UniqDestoryHash));
                    StringBuilder sBuilder = new StringBuilder();
                    for (int i = 0; i < data.Length; i++)
                    {
                        sBuilder.Append(data[i].ToString("x2"));
                    }
                    String destroyName = sBuilder.ToString();
                    return GlobalSettings.Instance.DestoryCharacterName ? destroyName : StyxWoW.Me.Name;
                }
            }
        }
        public String serverName
        {
            get { return StyxWoW.Me.RealmName; }
        }
        public String serverRegion
        {
            get { return Styx.WoWInternals.Lua.GetReturnVal<String>("return GetCVar(\"portal\")", 0); }
        }
    }
}
