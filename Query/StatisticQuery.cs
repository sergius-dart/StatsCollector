﻿using StatsCollector.Stats;
using Styx;

namespace StatsCollector.Query
{
    class StatisticQuery:BaseBotQuery
    {
        public StatsObject stats = new StatsObject();
    }

    class StatsObject
    {
        public HbStats hbstats = new HbStats();
        public WowStats wowstats = new WowStats();
        public GuildInfo guild = StyxWoW.Me.GuildLevel > 0 ? new GuildInfo() : null;
    }
}
