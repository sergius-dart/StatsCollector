﻿using System;

namespace StatsCollector.Query
{
    class BaseSecureQuery
    {
        public String accessToken
        {
            get { return SiteSync.Instance.AuthToken; }
        }
        public String privateKey
        {
            get { return SiteSync.DeviceId;  }
        }
    }
}
