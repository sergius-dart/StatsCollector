﻿using System;


namespace StatsCollector.Query
{
    /// <summary>
    /// Класс API для работы с сайтом
    /// </summary>
    class AuthQuery
    {
        public String login
        {
            get { return GlobalSettings.Instance.SyncLogin; }
        }
        public String pass
        {
            get { return GlobalSettings.Instance.SyncPassword; }
        }
        public String machineName
        {
            get { return GlobalSettings.Instance.SyncMachineName; }
        }
        public String machineId
        {
            get { return SiteSync.DeviceId; }
        }
    }
    public class AuthResponse
    {
        public String identy;
    }
}
