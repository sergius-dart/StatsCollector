﻿using System;
using System.ComponentModel;
using System.IO;
using Styx.Helpers;

namespace StatsCollector
{
    public class GlobalSettings : Settings
    {
        private static GlobalSettings _instance;
        public static GlobalSettings Instance { get { return _instance ?? (_instance = new GlobalSettings()); } }

        public GlobalSettings()
            : base(Path.Combine(Path.Combine(SettingsDirectory, "Settings"), "StatsCollectorSettings.xml"))
        {

        }

        [Setting]
        [Category("General")]
        [DisplayName("Global json-rpc url")]
        public String SyncAddress { get; set; }

        [Setting]
        [Category("General")]
        [DisplayName("Global name machine")]
        public String SyncMachineName { get; set; }


        [Setting]
        [Category("General")]
        [DisplayName("Global username")]
        public String SyncLogin { get; set; }

        [Setting]
        [Category("General")]
        [DisplayName("Global password")]
        public String SyncPassword { get; set; }

        [Setting]
        [Category("General")]
        [DisplayName("Added hash on destroy character name")]
        public String UniqDestoryHash { get; set; }

        [Setting]
        [Category("General")]
        [DisplayName("Destory character name ?")]
        public bool DestoryCharacterName { get; set; }
        

    }

}
